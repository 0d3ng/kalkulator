/*
 * Kalkulator

 * Copyright (c) 2020
 * All rights reserved.
 * Written by od3ng created on Dec 3, 2020 4:51:42 AM
 * Blog    : sinaungoding.com
 * Email   : lepengdados@gmail.com
 * Github  : 0d3ng
 * Hp      : 085878554150
 */
package kalkulator;

import java.util.Scanner;

/**
 *
 * @author od3ng
 */
public class Kalkulator {

    static void operasi(String operator) {
        Scanner s = new Scanner(System.in);
        while (true) {
            System.out.print("Masukan angka 1: ");
            int angka = s.nextInt();
            System.out.print("Masukan angka 2: ");
            int angka1 = s.nextInt();

            switch (operator) {
                case "+":
                    System.out.println(String.format("Hasilnya %d + %d adalah %d", angka, angka1, angka + angka1));
                    break;
                case "-":
                    System.out.println(String.format("Hasilnya %d - %d adalah %d", angka, angka1, angka - angka1));
                    break;
                case "*":
                    System.out.println(String.format("Hasilnya %d * %d adalah %d", angka, angka1, angka * angka1));
                    break;
                case "/":
                    System.out.println(String.format("Hasilnya %d / %d adalah %f", angka, angka1, (float) angka / (float) angka1));
                    break;
            }

            System.out.print("Apakah akan diulangi dengan angka yang lain?(Y/N)");
            char answer = s.next().charAt(0);
            if (answer == 'N' || answer == 'n') {
                break;
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int pilih = 0;
        do {
            System.out.println("*** Aplikasi kalkulator sederhana ***");
            System.out.println("Silakan akses menu di bawah ini");
            System.out.println("1. Operasi penambahan");
            System.out.println("2. Operasi pengurangan");
            System.out.println("3. Operasi perkalian");
            System.out.println("4. Operasi pembagian");
            System.out.println("5. Keluar");
            System.out.print("Pilih: ");
            pilih = s.nextInt();
            switch (pilih) {
                case 1:
                    operasi("+");
                    break;
                case 2:
                    operasi("-");
                    break;
                case 3:
                    operasi("*");
                    break;
                case 4:
                    operasi("/");
                    break;
            }
        } while (pilih != 5);
    }

}
